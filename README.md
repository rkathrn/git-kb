# Personal Git Knowledge Base

## Steps to Fork, Clone, and Checkout a New Branch

### Fork
1. Navigate to the project/repo you'll be working on
2. Select "Fork"
    * Gitlab: in the top right corner of a project/repo's main page
3. (GitLab) When prompted to pick a namespace to clone to: select your personal namespace. Any namespace you own, separate from the original project, will do.
4. Select "Fork"

### Clone to Local Machine (SSH Method)
1. Copy the SSH key to YOUR FORKED REPO
    * Gitlab: from the Project's main page, select "Clone" > "Clone with SSH"
    * selecting the clipboard icon next to "Clone with SSH" will automatically copy the SSH Key
2. Open a terminal on your local device
3. Navigate to where you want your cloned repo to live
    * best practice: put all code or git work into one folder on your computer
4. Enter the following command to clone your repo:

    ` git clone {copied repo SSH URL} `
5. Change directories INTO your newly-cloned repo

    `cd {repo name on local device}`
6. (VSCode) If using VSCode, use the following shortcut to open the app:

    `code . `
7. In your code editor (VSCode or other)- *but still in a terminal window* - enter the following terminal commands to configure Git
    * this username and email should match the github/gitlab account you're using - so any changes you make are correctly attributed to your handle

    ` git config user.name "Name"`

    ` git config user.email "email"`


### Connect Local Clone to the Original Upstream Repo
1. First check what upstream repositories your local copy is connected to already. Use the following command:

    ` git remote -v`
2. The previous command will show 'origin' - the repo you've forked to your own namespace. Now you need to add the original repo (from the project you're working with) as an 'upstream' repo. Enter the following command:

    `git remote add upstream {SSH URL for original repo}`

3. Enter the `git remote -v` command again. You should see the original repo listed with the word "upstream."

Amazing, you've now got a copy of your repository cloned on your device, connected to the project repo! Just one more step until you're ready to start making your changes.

### Create & Check out a New Branch
It's standard practice to make code or documentation changes in a branch. When you're ready, you'll merge your branch's edits with the main body.

1. To create a new branch and immediately check it out (move your working area to that branch), use the following command:

    `git checkout -b '{enter a descriptive branch name}`

2. Double-check you are working in the right branch by using the `git status` command. It will show:
    * where you are working (in what project and branch)
    * any changes you've made so far, and to what files
    * how many of your changes are staged or committed

I recommend frequently using `git status  ` to make sure all your hard work is going in the right place.


## Steps to Add, Commit, and Push New Branch Changes
So now you edit your repo - inside your branch in your local copy of a project. Now you're ready to push your edits to your forked repo, and then to the original project.

### Add Your Files to the Staging Area
* This stage won't change the file content in your online repo
* It will just signal to git that these file changes are going to be in the next commit

Use `git add /{File Name}` to add one file to the staging area
Use `git add -a` to add ALL files you've changed in this session to the staging area

If you enter `git status` again, you'll see that git has recognized and added those files.

### Commit Your Added (Staged) Changes to Your Branch
* Your changes won't go to your forked repo yet
* This just updates your branch
* Use the following command:
    
    `git commit -m "{a short, descriptive message describing what's been changed"}`

Your changes have now been saved (committed) to the branch you have been working in.

To push those branch changes to your forked repo copy, enter:
    ` git push origin {branch name} `

Congrats! The changes you made in your branch have now been pushed to your forked repo. 

If you're working on a collaborative open-source project, you'll want to open up a **Merge Request** or **Pull Request** to have your changes incorporated into the main repo, not just your clone.

If you're using GitLab, open up the original (shared) repo you've forked. You'll see a banner message at the top of the screen inviting you to open a merge request. Open it up.

Fill out the following information for a new Merge or Pull Request:
* Title
* Description
* Assignee (probably you)
* Reviewer (consult a project rep)

Each project or organization has their own etiquette around title naming conventions, description contents, and more. Plan accordingly!

At minimum, briefly describe the changes made. If there's an open issue or JIRA ticket for those changes, link it in the description.

Once you've submitted the request, someone will usually review and either approve the merge, or add comments for you to address.

## Fun Hacks
1. If you're working on a branch for a while and haven't yet committed changes, check for new changes to the original upstream repo on a regular basis:

    `git pull upstream main`

    This command will draw down any changes made to the source repo to your fork/local copy, keeping your edits up to date.

2. If your terminal window gets cluttered with a lot of text, enter `clear` to remove all previous command history
    (This won't undo your work, it'll just tidy up the terminal window.)


## Markdown Resources
[Markdown Guide Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
